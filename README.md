# node.js ms on kubertnetes

## ⏳ Requirements

- [`docker`](https://docs.docker.com/get-docker/)
- [`k3d >= v5.0.3`](https://k3d.io/#installation)
- [`Helm 3`](https://helm.sh/docs/intro/install/)

Optional - yet recommanded:

- [`kubectx + kubens`](https://github.com/ahmetb/kubectx) callable via the `kubens` binary
- [`Kustomize`](https://kubernetes-sigs.github.io/kustomize/installation/)
- [`chromium`](https://www.chromium.org/Home) callable via the `chromium` binary


## 🥅 Todays Goal's

Deploy a microservice architecture app

- redis
- api      (./src/api)
- consumer (./src/pinger)
- client   (./src/poller)

---

🆙 nso-prepreation - [redis](./docs/01-nso.md)
