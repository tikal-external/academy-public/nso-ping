# 1️⃣ Lab prep

> Get the `git-bundle` from your instuctor.

```sh
git clone <git-bundle-name>.bundle --branch=main nso-ping
```


## 1️⃣ prep K3D Cluster

## 🧹 Remmove/cleanup existing k3d clusters 

```sh
k3d cluster delete nodejs-demo
```

## Create k3d cluster named `nodejs-demo`

```sh
k3d cluster create nodejs-demo 
```

## Validate cluster is runnning 

```sh
kubectl cluster-info
```

Should yield somthing like the following:

```sh
Kubernetes control plane is running at https://0.0.0.0:52951
CoreDNS is running at https://0.0.0.0:52951/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
Metrics-server is running at https://0.0.0.0:52951/api/v1/namespaces/kube-system/services/https:metrics-server:/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

---
🆙 next deploy - [redis](./docs/02-redis.md)
