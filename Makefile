#!/usr/bin/make -f

# DOCKER_USERNAME:= hagzag
# REPO:= docker.io
# REGISTRY := $(REPO)/$(DOCKER_USERNAME)

DOCKER_COMPOSE := /usr/local/bin/docker-compose

.PHONY: k3d-devcluster k3d-cleanup-cluster dc-build dc-push dc-stop dc-clean-run dc-run
.PHONY: docker-check redis-container-start redis-container-stop redis-container-cleanup node-cleanup node-npm-install node-run-local 

# k3d crete devcluster
k3d-devcluster:
	@k3d cluster create devcluster \
	--api-port 127.0.0.1:6443 \
	-p 80:80@loadbalancer \
	-p 443:443@loadbalancer

k3d-cleanup-cluster:
	@k3d cluster delete devcluster

ping-kustomize-deploy:
	@kubectl apply -k ./deployment/manifests





# dc-* docker-compose

dc-build: 
	$(DOCKER_COMPOSE) build

dc-run: | dc-stop
	$(DOCKER_COMPOSE) up -d

dc-stop:
	$(DOCKER_COMPOSE) down

dc-run-clean: | dc-stop dc-build dc-run

# removed volumes !
dc-clean:
	$(DOCKER_COMPOSE) down --rmi local

# pushed to whatever the image: <repo>/<name> is set to ...
dc-push: 
	$(DOCKER_COMPOSE) push
